# Tools Package
A package that provides you Image to PDF, Merge PDF, Compress PDF and Compress Image and  So on.

## Currently supported features
* Convert Image to PDF
* Merge PDFs
* Reorder selected Images and PDF 
* Compress Image and PDF
* Save, Rename, Share and Delete File
* Supports  platforms (Android, IOS(Not Checked))

If you have any feature that you want to see in this package, please feel free to issue a suggestion. 🎉



## Documentation
## Getting Started
### Installation
- add this code in `pubspec.yaml`

```yaml
dependencies:
  tools_package:
    git: https://gitlab.com/kandelanil24/tools_package/
```
### Android Setup
#### Add this line to root/android/app/src/main/AndroidManifest.xml
```xml
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/> 
    <uses-permission android:name="android.permission.ACCESS_MEDIA_LOCATION"/>
    
<application .....
```



## Usage
Quick simple usage example:

#### Pick Images
```dart
 final images = await MeroTools.pickImages(
                        allowCompression: true,
                        allowMultiple: false,
                      );
```

#### Pick Files
```dart
  final files = await MeroTools.pickFiles(
                        allowCompression: true,
                        allowMultiple: false,
                      );
```

#### Get Recent Files
```dart
    List<File> files = await MeroTools.getFiles(Folder.imageToPdf);
```

#### Select image, reorder and convert
```dart
//Reorder Screen
ImageToPDFReorder(
        images,
        onDelete: (index) {
          setState(() {
            images.removeAt(index);
          });
        },
      ),

//Convert images to PDF
  File<File> file = await ImageToPdf.convert(context, images: images);

```
#### Select PDF and Merge
```dart
//Selected PDF and reorder Widget
SelectPDFPage(
        pdfs: pdfs,
        onDelete: (index) {
          pdfs.removeAt(index);
        },
      ),

//Merge PDF Function
 final fille = await MergePDF.merge(context, pdfPaths, Folder.mergePdf);
```
#### PDF Compress
```dart
final pdf = await PDFCompressor.compress(
                        context,
                        pdfFile: image.first,
                      );
```
#### Image Compress
```dart
 final file = await ImageCompressor.compress(
                        context: context,
                        image: image.first,
                      );
```

#### Recent Documents Preview
```dart
 RecentDocuments(
        documentType: DocumentType.image,
        files: files,
        foler: Folder.imageToPdf,
        onTap: (file) {
          // Here write your code to do something on tap
        },
        onShare:(file){
          // Herw write your code to share file
        }
        onRenameAndDelete:(){
            // Here write your code to get files again after rename and delete
        }
      ),
```

#### Document Types
```dart
 DocumentType.image,
 DocumentType.pdf,
 DocumentType.files,
```

#### Folders
```dart
 Folder.imageToPdf,
 Folder.mergePdf,
 Folder.pdfCompress,
 Folder.imageCompress,
```

## LICENSE
    Copyright (c) 2024 Girija Prasad Kandel
