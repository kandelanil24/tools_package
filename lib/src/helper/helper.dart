import 'dart:developer';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tools_package/src/index.dart';
import 'package:path_provider/path_provider.dart' as path;

/// This file contains all the functions
/// used in the app.
/// This file is part of the Tools Package project.

class MeroTools {
  ///This function is used to get Temporary Directory and returns
  ///path for save file with file name
  static Future<String> getTemporaryDirectory(
      {required String ext, String? folder, String? outputName}) async {
    try {
      final tempPath = await path.getApplicationDocumentsDirectory();
      final savepath = Directory('${tempPath.path}/$folder');
      if (!savepath.existsSync()) {
        savepath.createSync();
      }
      final outputPath = '${savepath.path}/${outputName ?? 'output'}.$ext';
      return outputPath;
    } catch (e, stackTrace) {
      log("Error: $e", error: e, stackTrace: stackTrace);
      throw ToolsException("Error in getting temporary directory: $e");
    }
  }

  ///This function will return all files in the given path
  static Future<List<File>> getFiles(Folder folder) async {
    final directory = (await path.getApplicationDocumentsDirectory()).path;
    final folderName = getFolderName(folder);
    if (!Directory(directory).existsSync()) {
      return [];
    }
    final files = Directory("$directory/$folderName/")
        .listSync(); //use your folder name insted of resume.

    files.sort((a, b) {
      final aStat = File(a.path).statSync();
      final bStat = File(b.path).statSync();
      return bStat.modified.compareTo(aStat.modified);
    });
    return files.cast<File>().toList();
  }

  ///This function is used to pick images from gallery
  ///and return [images] which is List<File>

  static Future<List<File>> pickImages(
      {bool? allowMultiple, bool? allowCompression}) async {
    try {
      List<File> pickedFiles = [];
      final result = await FilePicker.platform.pickFiles(
        allowCompression: allowCompression ?? true,
        allowMultiple: allowMultiple ?? false,
        type: FileType.image,
      );

      if (result != null && result.files.isNotEmpty) {
        for (PlatformFile file in result.files) {
          File fi = File(file.path ?? '');
          if (await fi.exists()) {
            pickedFiles.add(fi);
          }
        }
      }
      return pickedFiles;
    } catch (e, stackTrace) {
      log("Error: $e", error: e, stackTrace: stackTrace);
      throw ToolsException("Error picking images: $e");
    }
  }

  ///This function is used to pick images from gallery
  ///and return [Files] which is List<File>

  static Future<List<File>> pickFiles(
      {bool? allowMultiple, bool? allowCompression}) async {
    try {
      List<File> pickedFiles = [];
      final result = await FilePicker.platform.pickFiles(
        allowCompression: allowCompression ?? true,
        allowMultiple: allowMultiple ?? false,
        allowedExtensions: ['pdf'],
        type: FileType.custom,
      );

      if (result != null && result.files.isNotEmpty) {
        for (PlatformFile file in result.files) {
          File fi = File(file.path ?? '');
          if (await fi.exists()) {
            pickedFiles.add(fi);
          }
        }
      }
      return pickedFiles;
    } catch (e, stackTrace) {
      log("Error: $e", error: e, stackTrace: stackTrace);
      throw ToolsException("Error picking images: $e");
    }
  }

  ///This function will delete file in the given path
  static Future<List<File>> deleteFile(String filePath,
      {String? folderName}) async {
    final directory = (await path.getApplicationDocumentsDirectory()).path;
    final mergePdfDirectory = Directory('$directory/$folderName/');

    if (!mergePdfDirectory.existsSync()) {
      return [];
    }
    try {
      if (await File(filePath).exists()) {
        await File(filePath).delete();
        log("Deleted");
        Fluttertoast.showToast(
          msg: "Deleted Successfully",
          backgroundColor: Colors.green.shade700,
        );
      }
    } catch (e) {
      Fluttertoast.showToast(
        msg: "Error on Deleted",
        backgroundColor: Colors.red.shade700,
      );
    }

    final file = Directory("$directory/$folderName/").listSync();
    return file.cast<File>().toList();
  }

  ///This function will delete file in the given path
  static Future<void> renameFile(String oldFilePath, String newFileName) async {
    final File oldFile = File(oldFilePath);
    final Directory parentDirectory = oldFile.parent;
    final ext = oldFilePath.split('/').last.split('.').last;

    if (await oldFile.exists()) {
      final String newFilePath = '${parentDirectory.path}/$newFileName.$ext';
      await oldFile.rename(newFilePath);
    } else {
      log('File not found: $oldFilePath');
    }
  }

  ///Show dialog eith text field
  static Future<void> customDialog(
    BuildContext context, {
    required GlobalKey<FormState> formKey,
    required TextEditingController controller,
    String? title,
    String? description,
    required Function onDone,
  }) async {
    final date = DateTime.now();
    final formattedDate = DateFormat('yyyy-MM-dd_hh-mm-ss').format(date);
    controller.text = "edusmarttools-$formattedDate";
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          title: Text(
            "$title",
            textAlign: TextAlign.center,
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "$description",
                style: const TextStyle(
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 5),
              Form(
                key: formKey,
                child: TextFormField(
                  controller: controller,
                  decoration: InputDecoration(
                    hintText: 'merofile',
                    contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Colors.grey.shade200,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Colors.grey.shade200,
                      ),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: const BorderSide(
                        color: Colors.red,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Colors.grey.shade200,
                      ),
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: const BorderSide(
                        color: Colors.red,
                      ),
                    ),
                  ),
                  validator: (val) {
                    if (val == '') {
                      return 'This field cannot be empty.';
                    }
                    return null;
                  },
                ),
              ),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue.shade900,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text('Cancel'),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue.shade900,
                    ),
                    onPressed: () async {
                      if (formKey.currentState!.validate()) {
                        onDone().then((_) {
                          Navigator.pop(context);
                        });
                      }
                    },
                    child: const Text('Done'),
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  ///Show dialog for confirmation
  static Future<void> confirmationDialog(
    BuildContext context, {
    String? title,
    required Function onYes,
  }) async {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          title: Text(
            "$title",
            textAlign: TextAlign.center,
          ),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue.shade900,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text('No'),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue.shade900,
                    ),
                    onPressed: () async {
                      onYes().then((_) {
                        Navigator.pop(context);
                      });
                    },
                    child: const Text('Yes'),
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  ///This function will save file to phone directory from app directory
  static Future<void> saveFileToPhoneDirectory(String filePath) async {
    try {
      final fname = filePath.split('/').last;

      var status = await Permission.storage.status;
      if (!status.isGranted) {
        status = await Permission.storage.request();
      }
      final downloadsPath = '/storage/emulated/0/Download/$fname';
      final file = File(filePath);
      await file.copy(downloadsPath);
      Fluttertoast.showToast(
        msg: "Saved Successfully",
        backgroundColor: Colors.green.shade700,
      );
    } catch (e) {
      ToolsException("Error :: $e");
      Fluttertoast.showToast(
        msg: "Error on Save",
        backgroundColor: Colors.red.shade700,
      );
      log('Error saving file: $e');
    }
  }
}
