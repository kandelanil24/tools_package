///Document Type define Which Icon will be shown in Document List
enum DocumentType {
  pdf,
  image,
  file,
}

enum Folder {
  imageToPdf,
  mergePdf,
  pdfCompress,
  imageCompress,
}

getFolderName(Folder folder) {
  switch (folder) {
    case Folder.imageToPdf:
      return 'imagetopdf';
    case Folder.mergePdf:
      return 'mergedpdf';
    case Folder.pdfCompress:
      return 'compressedpdf';
    case Folder.imageCompress:
      return 'compressedimage';
    default:
      return 'others';
  }
}
