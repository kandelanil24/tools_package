import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pdf_merger/pdf_merger.dart';
import 'package:tools_package/tools_package.dart';

class MergePDF {
  /// This function will merge multiple PDF and returns a combined PDF Path
  /// It receives a List of PDF's Path and combined them
  static Future<File> merge(
      BuildContext context, List<String> pdfFilePaths, Folder folder) async {
    try {
      if (pdfFilePaths.isEmpty) {
        throw ToolsException("No PDF selected.");
      }
      File mergedFile = File('');

      ///Showing Saving File dialog
      GlobalKey<FormState> formKey = GlobalKey();
      TextEditingController controller = TextEditingController();
      if (context.mounted) {}
      await MeroTools.customDialog(context,
          title: "Save File",
          description: "Enter Filename",
          formKey: formKey,
          controller: controller, onDone: () async {
        final tempPath = await MeroTools.getTemporaryDirectory(
          ext: 'pdf',
          folder: getFolderName(folder),
          outputName: controller.text,
        );

        MergeMultiplePDFResponse response = await PdfMerger.mergeMultiplePDF(
            paths: pdfFilePaths, outputDirPath: tempPath);

        if (response.status == "success") {
          mergedFile = File(tempPath);
        } else {
          mergedFile = File('');
        }
      });
      if (mergedFile.path != '') {
        return mergedFile;
      }
      throw ToolsException("Error on converting images to PDF.");
    } catch (e) {
      throw ToolsException("Error: $e");
    }
  }
}
