import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

///This page Will show the selected PDF and
///Provide a facility to arrange the PDF orders

/// List[pdfs] -> Selected Local PDFs
/// [onDelete] Functions will be Returned function which will return index of file

class SelectPDFPage extends StatefulWidget {
  final List<File> pdfs;
  final Function onDelete;
  const SelectPDFPage({required this.pdfs, required this.onDelete, super.key});

  @override
  State<SelectPDFPage> createState() => _SelectPDFPageState();
}

class _SelectPDFPageState extends State<SelectPDFPage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
      child: widget.pdfs.isEmpty
          ? const Center(
              child: Text("No PDF selected"),
            )
          : ReorderableListView.builder(
              itemBuilder: (context, index) {
                final currentFileName =
                    widget.pdfs[index].path.split('/').last.split('.').first;
                return Card(
                  key: ValueKey(widget.pdfs[index].path),
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        color: Colors.grey.shade200,
                      )),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                'assets/icons/pdf.svg',
                                package: 'tools_package',
                              ),
                              const SizedBox(width: 20),
                              Expanded(
                                child: Text(
                                  currentFileName,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                        ),
                        IconButton(
                          onPressed: () => widget.onDelete(index),
                          icon: const Icon(
                            Icons.delete_forever_outlined,
                            color: Colors.red,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              itemCount: widget.pdfs.length,
              onReorder: (a, b) {
                if (a < b) {
                  b -= 1;
                }
                final item = widget.pdfs.removeAt(a);
                widget.pdfs.insert(b, item);
              },
            ),
    );
  }
}
