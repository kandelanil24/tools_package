class ToolsException implements Exception {
  final String message;

  ToolsException(this.message);

  @override
  String toString() => message;
}
