import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:tools_package/tools_package.dart';

// import 'package:path_provider/path_provider.dart' as path;

/// This file contains all the functions to convert compress PDF.

class ImageCompressor {
  /// This function will Compress [PDF]
  /// and return a compressed PDF file which will saved in temprorary directory
  static Future<File> compress(
      {required BuildContext context,
      required File image,
      int quality = 50}) async {
    try {
      if (image.path == '') {
        throw ToolsException("No image selected");
      }
      XFile? file = XFile('');
      final ext = image.path.split('.').last;

      GlobalKey<FormState> formKey = GlobalKey();
      TextEditingController controller = TextEditingController();
      if (context.mounted) {
        await MeroTools.customDialog(
          context,
          title: "Save File",
          description: "Enter File Name",
          formKey: formKey,
          controller: controller,
          onDone: () async {
            final outputPath = await MeroTools.getTemporaryDirectory(
                ext: ext,
                folder: getFolderName(Folder.imageCompress),
                outputName: controller.text);
            // final outputPath = '$tempPath/${controller.text}.$ext';
            file = await FlutterImageCompress.compressAndGetFile(
              image.absolute.path,
              outputPath,
              quality: quality,
            );
          },
        );
      }
      if (file?.path == '') {
        throw ToolsException("Something went wrong.");
      }
      return File(file!.path);
    } catch (e) {
      throw ToolsException("Error: $e");
    }
  }
}
