import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:tools_package/src/index.dart';

/// This file contains all the functions to convert images to PDF.

class ImageToPdf {
  /// This function will convert [images] which is List<File> to PDF
  /// and return a PDF file which will saved in temprorary directory

  static Future<File> convert(BuildContext context,
      {required List<File> images}) async {
    try {
      if (images.isEmpty) {
        throw ToolsException("Atleast one Image should be selected.");
      }
      final pdf = pw.Document();
      for (final imagePath in images) {
        final imageFile = File(imagePath.path);
        final imageBytes = imageFile.readAsBytesSync();

        /// Add an image to the PDF
        pdf.addPage(
          pw.Page(
            build: (pw.Context context) {
              return pw.Center(
                child: pw.Image(pw.MemoryImage(imageBytes)),
              );
            },
          ),
        );
      }
      final pdfBytes = await pdf.save();
      File pdfFile = File('');

      ///Showing Saving File dialog
      GlobalKey<FormState> formKey = GlobalKey();
      TextEditingController controller = TextEditingController();
      if (context.mounted) {}
      await MeroTools.customDialog(
        context,
        title: "Save File",
        description: "Enter Filename",
        formKey: formKey,
        controller: controller,
        onDone: () async {
          final tempPath = await MeroTools.getTemporaryDirectory(
            ext: 'pdf',
            folder: getFolderName(Folder.imageToPdf),
            outputName: controller.text,
          );
          pdfFile = File(tempPath);
          pdfFile.writeAsBytesSync(pdfBytes);
          return pdfFile;
        },
      );
      if (pdfFile.path == '') {
        throw ToolsException("message");
      }
      return pdfFile;
    } catch (e) {
      throw ToolsException("Error: $e");
    }
  }
}
