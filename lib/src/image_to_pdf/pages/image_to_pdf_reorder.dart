import 'dart:io';

import 'package:flutter/material.dart';
import 'package:reorderable_grid/reorderable_grid.dart';

///This page Will show the selected Images

/// List[images] -> Selected Local Images
/// [onDelete] Functions will be Returned function which will return [index] of file

class ImageToPDFReorder extends StatelessWidget {
  final List<File> images;
  final Function(int index) onDelete;
  const ImageToPDFReorder(this.images, {required this.onDelete, super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.02),
      child: images.isEmpty
          ? const Center(
              child: Text('No images selected.'),
            )
          : ReorderableGridView.builder(
              itemCount: images.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
              ),
              onReorder: (a, b) {
                if (a < b) {
                  b -= 1;
                }
                final item = images.removeAt(a);
                images.insert(b, item);
              },
              itemBuilder: (context, index) {
                final currentImage = images[index].path;
                return Stack(
                  key: ValueKey(images[index].path),
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      height: MediaQuery.of(context).size.height * 0.2,
                      width: MediaQuery.of(context).size.height * 0.2,
                      child: Image.file(
                        File(currentImage),
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.05,
                      width: MediaQuery.of(context).size.height * 0.05,
                      decoration: BoxDecoration(
                        color: Colors.red.withOpacity(0.3),
                        borderRadius: const BorderRadius.only(
                          bottomRight: Radius.circular(10),
                        ),
                      ),
                      child: IconButton(
                        onPressed: () => onDelete(index),
                        icon: const Icon(
                          Icons.delete_forever,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
    );
  }
}
