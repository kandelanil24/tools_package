import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pdf_compressor/pdf_compressor.dart';
import 'package:tools_package/tools_package.dart';

// /// This file contains all the functions to convert compress PDF.

class PDFCompressor {
  // static const MethodChannel _channel = MethodChannel('pdf_compressor');

  /// This function will Compress [PDF]
  /// and return a compressed PDF file which will saved in temprorary directory
  static Future<File> compress(
    BuildContext context, {
    required File pdfFile,
  }) async {
    try {
      if (pdfFile.path == '') {
        throw ToolsException("No PDF Selected.");
      }
      String outputPath = '';

      GlobalKey<FormState> formKey = GlobalKey();
      TextEditingController controller = TextEditingController();
      if (context.mounted) {}
      await MeroTools.customDialog(
        context,
        title: "Save File",
        description: "Enter Filename",
        formKey: formKey,
        controller: controller,
        onDone: () async {
          outputPath = await MeroTools.getTemporaryDirectory(
            ext: 'pdf',
            folder: getFolderName(Folder.pdfCompress),
            outputName: controller.text,
          );
          await PdfCompressor.compressPdfFile(
              pdfFile.path, outputPath, CompressQuality.HIGH);
        },
      );
      if (outputPath == '') {
        throw ToolsException("Something went wrong");
      }
      return File(outputPath);
    } catch (e) {
      throw ToolsException("Error: $e");
    }
  }
}
