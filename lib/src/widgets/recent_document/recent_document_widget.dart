import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:tools_package/src/helper/enums.dart';
import 'package:tools_package/src/helper/helper.dart';

///This is a widget for showing a List of Which require [files] (Local File), [documentType]

class RecentDocuments extends StatelessWidget {
  final List<File> files;
  final Function(File) onTap;
  final DocumentType documentType;
  final Folder folder;
  final Function? onRenameAndDelete;
  final Function(File)? onShare;
  const RecentDocuments(
      {super.key,
      required this.files,
      required this.folder,
      required this.onTap,
      this.onRenameAndDelete,
      this.onShare,
      this.documentType = DocumentType.file});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Recent documents",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(height: 4),
            Expanded(
              child: files.isEmpty
                  ? const Center(
                      child: Text("No recent Files"),
                    )
                  : ListView.builder(
                      physics: const BouncingScrollPhysics(),
                      itemCount: files.length,
                      itemBuilder: (context, index) {
                        final currentFile = files[index];
                        return GestureDetector(
                          onTap: () => onTap(currentFile),
                          child: Card(
                            elevation: 0,
                            clipBehavior: Clip.antiAlias,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                              side: BorderSide(
                                color: Colors.grey.shade200,
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Flexible(
                                    child: Row(
                                      children: [
                                        Container(
                                          child: documentType ==
                                                  DocumentType.image
                                              ? Image.file(
                                                  currentFile,
                                                  height: 50,
                                                  width: 50,
                                                )
                                              : documentType == DocumentType.pdf
                                                  ? SvgPicture.asset(
                                                      'assets/icons/pdf.svg',
                                                      package: 'tools_package',
                                                    )
                                                  : SvgPicture.asset(
                                                      'assets/icons/file.svg',
                                                      package: 'tools_package',
                                                    ),
                                        ),
                                        const SizedBox(width: 20),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                currentFile.path
                                                    .split('/')
                                                    .last,
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w700,
                                                ),
                                              ),
                                              const SizedBox(height: 5),
                                              Text(
                                                '${getSize(currentFile.lengthSync())} / ${formatISOString(currentFile.lastModifiedSync().toIso8601String())}',
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        PopupMenuButton<String>(
                                          elevation: 1,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              side: BorderSide(
                                                color: Colors.grey.shade200,
                                              )),
                                          onSelected: (String value) async {
                                            if (value == 'save') {
                                              MeroTools
                                                  .saveFileToPhoneDirectory(
                                                      currentFile.path);
                                            }
                                            if (value == 'rename') {
                                              final filename = currentFile.path
                                                  .split('/')
                                                  .last
                                                  .split('.')
                                                  .first;
                                              GlobalKey<FormState> formKey =
                                                  GlobalKey();
                                              TextEditingController controller =
                                                  TextEditingController(
                                                      text: filename);
                                              await MeroTools.customDialog(
                                                context,
                                                formKey: formKey,
                                                controller: controller,
                                                title: "Rename File",
                                                description: "Enter file name",
                                                onDone: () async {
                                                  await MeroTools.renameFile(
                                                    currentFile.path,
                                                    controller.text,
                                                  );
                                                },
                                              ).then(
                                                (value) =>
                                                    onRenameAndDelete?.call(),
                                              );
                                            }
                                            if (value == 'share') {
                                              onShare?.call(currentFile);
                                            }
                                            if (value == 'delete') {
                                              if (context.mounted) {}
                                              MeroTools.confirmationDialog(
                                                context,
                                                title:
                                                    "Are you sure want to delete?",
                                                onYes: () async {
                                                  await MeroTools.deleteFile(
                                                    currentFile.path,
                                                    folderName:
                                                        getFolderName(folder),
                                                  ).then(
                                                    (value) => onRenameAndDelete
                                                        ?.call(),
                                                  );
                                                },
                                              );
                                            }
                                          },
                                          itemBuilder: (BuildContext context) =>
                                              <PopupMenuEntry<String>>[
                                            PopupMenuItem<String>(
                                              value: 'save',
                                              child: Row(
                                                children: const [
                                                  Icon(Icons.save),
                                                  SizedBox(width: 6),
                                                  Text("Save"),
                                                ],
                                              ),
                                            ),
                                            PopupMenuItem<String>(
                                              value: 'rename',
                                              child: Row(
                                                children: const [
                                                  Icon(Icons.edit),
                                                  SizedBox(width: 6),
                                                  Text("Rename"),
                                                ],
                                              ),
                                            ),
                                            PopupMenuItem<String>(
                                              value: 'share',
                                              child: Row(
                                                children: const [
                                                  Icon(Icons.share),
                                                  SizedBox(width: 6),
                                                  Text("Share"),
                                                ],
                                              ),
                                            ),
                                            PopupMenuItem<String>(
                                              value: 'delete',
                                              child: Row(
                                                children: const [
                                                  Icon(Icons.delete),
                                                  SizedBox(width: 6),
                                                  Text("Delete"),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
            )
          ],
        ),
      ),
    );
  }

  String getSize(int bytes) {
    if (bytes < 1024) {
      return '$bytes Bytes';
    } else if (bytes < 1024 * 1024) {
      return '${(bytes / 1024).toStringAsFixed(2)} KB';
    } else if (bytes < 1024 * 1024 * 1024) {
      return '${(bytes / (1024 * 1024)).toStringAsFixed(2)} MB';
    } else {
      return '${(bytes / (1024 * 1024 * 1024)).toStringAsFixed(2)} GB';
    }
  }

  String formatISOString(String isoString) {
    DateTime dateTime = DateTime.parse(isoString);
    String formattedDate = DateFormat('MM-dd-yyyy HH:mm').format(dateTime);
    return formattedDate;
  }
}
