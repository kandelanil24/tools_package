import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MeroAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;
  MeroAppBar({
    super.key,
    required this.title,
    this.action,
    this.leading,
    double? preferredSize,
    this.backgroundColor,
    this.statusBarColor,
    this.autoLeading,
    this.bottom,
  }) : preferredSize = Size.fromHeight(preferredSize ?? kToolbarHeight);
  final String title;
  final List<Widget>? action;
  final Widget? leading;
  final Widget? bottom;
  final Color? backgroundColor;
  final Color? statusBarColor;
  final bool? autoLeading;
  @override
  Widget build(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: statusBarColor ?? Colors.blue.shade900,
          statusBarBrightness: Brightness.light),
      title: Text(title),
      automaticallyImplyLeading: autoLeading ?? true,
      elevation: 0,
      backgroundColor: backgroundColor ?? Colors.blue.shade900,
      actions: action,
      leading: leading,
      bottom: bottom == null
          ? null
          : PreferredSize(
              preferredSize: preferredSize,
              child: bottom!,
            ),
    );
  }
}
